package org.cosyverif.service.adt2amas;

import org.cosyverif.alligator.service.AnnotatedService.Task;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.ParameterOutputHandler;
import org.cosyverif.model.Model;

@Service(name = "EAMAS Translator",
         help = "Translates an ADTree model into its EAMAS model",
         version = "1.0.0",
         tool = "adt2amas",
         index = 1,
         authors = {
            "Jaime Arias",
            "Laure Petrucci",
            "Wojciech Penczek",
            "Teofil Sidoruk"
        },
        keywords = {})
public class EAMASTranslation extends BinaryService{
  GrmlParser parser;

  @Parameter(name = "Model parameter",
            help = "Attack-Defence Tree model",
            direction = Direction.IN,
            formalism = "http://formalisms.cosyverif.org/adtree.fml")
  private Model modelParameter;

  @Parameter(name = "LaTeX file",
            help = "LaTeX file of the EAMAS model",
            direction = Direction.OUT,
            contenttype = "plain/text")
  private File latexFile = null;

  @Parameter(name = "IMITATOR file",
            help = "IMITATOR file of the EAMAS model",
            direction = Direction.OUT,
            contenttype = "plain/text")
  private File imitatorFile = null;

  @Launch
  public Task executeBinary() throws IOException  {
    parser = GrmlParser.create(modelParameter);
    final File model = parser.parse();

    String binaryPath = Paths.get(baseDirectory().getAbsolutePath(), "adt2amas").toString();

    final File runnable = new File(binaryPath);
    runnable.setExecutable(true);

    final CommandLine command = new CommandLine("./adt2amas");
    command.addArguments("transform --model " + model.getName());

    return task(command)
            .workingDirectory(baseDirectory())
            .out(new ParameterOutputHandler(this){
                @Override
                public void fallback(String line){
                  try{
                    getOutputFiles();
                  } catch (IOException e) {
                    e.printStackTrace();
                  }
                }

                @Override
                public void fallback(String key, String value) {
                }
            });
  }

  private void getOutputFiles() throws IOException {
    File directory = new File(baseDirectory().getAbsolutePath());

    File[] files = directory.listFiles(new FilenameFilter() {
        public boolean accept(File dir, String name) {
            return name.endsWith(".imi") || name.endsWith(".tex");
        }
    });
    Arrays.sort(files);

    imitatorFile = files[0];
    latexFile =  files[1];
  }

  @Example(name = "Example of the EAMAS translation",
          help = "Example of the EAMAS translation")
  public EAMASTranslation example() {
    EAMASTranslation result = new EAMASTranslation();

    result.modelParameter = loadModelResource("treasure-hunters.grml");
    return result;
  }
}
