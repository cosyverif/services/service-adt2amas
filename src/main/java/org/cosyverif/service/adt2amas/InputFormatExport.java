package org.cosyverif.service.adt2amas;

import java.io.File;
import java.io.IOException;

import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.model.Model;

@Service(name = "ADT2AMAS Format Export",
         help = "Export ADTree model into the ADT2AMAS format",
         version = "1.0.0",
         tool = "adt2amas",
         index = 3,
         authors = {
          "Jaime Arias",
          "Laure Petrucci",
          "Wojciech Penczek",
          "Teofil Sidoruk"
         })
public class InputFormatExport extends BinaryService {
  GrmlParser parser;

  @Parameter(name = "Model parameter",
            help = "Attack-Defence Tree model",
            direction = Direction.IN,
            formalism = "http://formalisms.cosyverif.org/adtree.fml")
  private Model modelParameter;

  @Parameter(name = "ADT2AMAS input file",
            help = "ADTree model written with the ADT2AMAS format",
            direction = Direction.OUT,
            contenttype = "plain/text")
  private File fileParameter = null;

  @Launch
  public Task executeBinary() throws IOException  {
    parser = GrmlParser.create(modelParameter);
    fileParameter = parser.parse();

    return null;
  }

  @Example(name = "Example of the input format export",
          help = "Example of the input format export")
  public InputFormatExport example() {
    InputFormatExport result = new InputFormatExport();

    result.modelParameter = loadModelResource("treasure-hunters.grml");
    return result;
  }
}
