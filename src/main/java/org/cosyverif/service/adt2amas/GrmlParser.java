package org.cosyverif.service.adt2amas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.val;
import lombok.experimental.Accessors;

import org.cosyverif.Configuration;
import org.cosyverif.model.Model;
import org.cosyverif.model.Node;
import org.cosyverif.model.Arc;
import org.cosyverif.model.Attribute;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.NONE)
@Accessors(chain = true,
           fluent = true)
public class GrmlParser {
  /** GRML Model */
  private Model model;

  /** Map containing all the ADTree nodes <id, node> */
  private HashMap<String, ADTreeNode> mapNodes = new HashMap<String, ADTreeNode>();

  /**
   * Constructor
   *
   * @param model Grml Model
   */
  private GrmlParser(Model model) {
    this.model = model;
  }

  /**
   * Creates a parser from Grml to ADTree
   *
   * @param model Grml Model
   * @return GrmlParser object
   */
  public static GrmlParser create(Model model) {
    return new GrmlParser(model);
  }

  /**
   * Generates a file handled by the adt2amas tool
   *
   * @return File object of the generated file
   * @throws IOException
   */
  public File parse() throws IOException {
    File fileOutput = null;
    BufferedWriter bw = null;
    FileWriter fw = null;

    try {
      File outputDirectory = Configuration.instance().temporaryDirectory();
      fileOutput = File.createTempFile("adtree_", "", outputDirectory);
      fw = new FileWriter(fileOutput);
      bw = new BufferedWriter(fw);

      /** Parsing root node */
      String root = "";
      for (Attribute attr : model.getAttribute()) {
        if (attr.getName().equals("root") && attr.getContent().size() == 1) {
          root = (String) attr.getContent().get(0);
        }
      }

      /* parsing nodes */
      for (Node node : model.getNode()) {
        HashMap<String, String> attrs = new HashMap<String, String>();
        String id = node.getId().toString();

        // default values of attributes
        attrs.put("time", "0");
        attrs.put("cost", "0");

        attrs.put("type", node.getNodeType());

        // Parse attributes
        for (Object content : node.getNodeContent()) {
          if (content instanceof Attribute) {
            val attr = Attribute.class.cast(content);

            String attributeName = attr.getName();
            List<Object> attributeContent = attr.getContent();

            if (attributeContent.size() == 0) {
              throw new Exception("Empty Attribute");
            } else if (attributeContent.size() == 1) {
              String value = (String) attributeContent.get(0);
              attrs.put(attributeName, value);
            } else if (attributeName.equals("attributes")) {
              attrs.putAll(getAttributes(attr));
            }
          }
        }

        // create ADTree Node
        ADTreeNode adtreeNode = new ADTreeNode(id, attrs.get("type").toLowerCase(),
            attrs.get("name").replaceAll("\\s+", "_"), attrs.get("role").toLowerCase(), attrs.get("time"),
            attrs.get("cost"), new ArrayList<String>());

        mapNodes.put(id, adtreeNode);
      }

      /* parsing arcs */
      for (Arc arc : model.getArc()) {
        if (arc.getArcType().equals("arc")) {
          String source = arc.getSource().toString();
          String target = arc.getTarget().toString();
          mapNodes.get(source).addChild(target);
        }
      }

      /** Create ADT2AMAS input file */
      bw.write(mapNodes.get(root).name() + "\n");

      for (Map.Entry element : mapNodes.entrySet()) {
        ADTreeNode node = (ADTreeNode) element.getValue();
        bw.write("\n" + node.name() + " " + node.type() + " " + node.role() + " " + node.cost() + " " + node.time());
      }
      bw.newLine();

      Queue<String> queue = new ArrayDeque<String>();
      queue.add(root);
      while (!queue.isEmpty()) {
        String node = queue.remove();
        ArrayList<String> children = mapNodes.get(node).chidlren();

        if (!children.isEmpty()) {
          queue.addAll(children);
          bw.write("\n" + mapNodes.get(node).name());
          for (String child : children) {
            bw.write(" " + mapNodes.get(child).name());
          }
        }
      }
      bw.write("\n\n");
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (bw != null) bw.close();
      if (fw != null) fw.close();
    }

    return fileOutput;
  }

  /**
   * Parses a Grml node containing ADTree attributes (e.g. time, cost)
   *
   * @param node Grml Node
   * @return Map<name, value> with the attribute's information
   */
  private HashMap<String, String> getAttributes(Attribute node) {
    HashMap<String, String> attrs = new HashMap<String, String>();

    // Group of attributes
    for (Object object : node.getContent()) {
      if (object instanceof Attribute) {
        val attribute = Attribute.class.cast(object);
        List<Object> attributeContent = attribute.getContent();

        // it's a valid ADTree attribute
        if (attribute.getName().equals("attribute")) {
          HashMap<String, String> tmp = new HashMap<String, String>();

          for (Object attrObject : attributeContent) {
            if (attrObject instanceof Attribute) {
              val attrInfo = Attribute.class.cast(attrObject);
              List<Object> attrContent = attrInfo.getContent();

              // well-formed attribute
              if (attrContent.size() == 1) {
                tmp.put(attrInfo.getName(), (String) attrContent.get(0));
              }
            }
          }

          // save attribute
          if (tmp.containsKey("name") && tmp.containsKey("value")) {
            attrs.put(tmp.get("name"), tmp.get("value"));
          }
        }
      }
    }
    return attrs;
  }

}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true,
          fluent = true)
class ADTreeNode {
  /** Identifier of the node */
  private String id;

  /** Type of the node */
  private String type;

  /** Name of the node */
  private String name;

  /** Role of the node */
  private String role;

  /** Time attribute of the node */
  private String time;

  /** Cost attribute of the node */
  private String cost;

  /** Children of the node */
  private ArrayList<String> chidlren;

  /**
   * Add a new child to the node
   *
   * @param child new child
   */
  public void addChild(String child) {
    this.chidlren.add(child);
  }
}
