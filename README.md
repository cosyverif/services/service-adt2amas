# adt2amas Service

# Dependencies

- maven

# Compilation

- `mvn compile`

# Installation

- `mvn install`

# Development

You can use the
[Alligator](https://depot.lipn.univ-paris13.fr/cosyverif/alligator/alligator)
Docker image as development environment.

To run tests you can use the tool [soapui](https://www.soapui.org/). The
initial WSDL is http://localhost:9000/services?wsdl. An example is shown
below:


```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:all="http://alligator.cosyverif.org/">
  <soapenv:Header/>
  <soapenv:Body>
    <all:asynchronous-call xmlns:ns2="http://alligator.cosyverif.org/" xmlns:ns3="http://cosyverif.org/ns/model" xmlns:ns4="http://cosyverif.org/ns/formalism">
      <service>
        <authors>
          <authors>Jaime Arias</authors>
          <authors>Laure Petrucci</authors>
        </authors>
        <help>Computes the minimal attack time using minimal number of agents for a given attack-defence tree</help>
        <identifier>org.cosyverif.service.adt2amas.MinimalScheduling</identifier>
        <isOldService>false</isOldService>
        <keywords/>
        <name>Minimal Schedule with Minimal Number of Agents in Attack-Defence Trees</name>
        <parameters>
          <parameters xsi:type="ns2:modelParameter" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <name>Model parameter</name>
            <shortname/>
            <help>Attack-Defence Tree model</help>
            <direction>IN</direction>
            <editable>false</editable>
            <formalism>
              <formalism>http://formalisms.cosyverif.org/adtree.fml</formalism>
            </formalism>
            <model formalismUrl="adtree.fml"
                  xmlns="http://cosyverif.org/ns/model">

              <!-- root gate -->
              <attribute name="root">9</attribute>

              <!-- ================================================ -->
              <!-- Leaves                                           -->
              <!-- ================================================ -->

              <!-- bribe gatekeeper (leaf b) -->
              <node id="1" nodeType="leaf">
                <attribute name="name">bribe gatekeeper</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">500</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">60</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- force arm. door (leaf f) -->
              <node id="2" nodeType="leaf">
                <attribute name="name">force arm. door</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">100</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">120</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- helicopter (leaf h) -->
              <node id="3" nodeType="leaf">
                <attribute name="name">helicopter</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">500</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">3</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- emergency exit (leaf e) -->
              <node id="4" nodeType="leaf">
                <attribute name="name">emergency exit</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">10</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- police (leaf p) -->
              <node id="5" nodeType="leaf">
                <attribute name="name">police</attribute>
                <attribute name="role">DEFENCE</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">100</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">10</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- ================================================ -->
              <!-- Gates                                            -->
              <!-- ================================================ -->

              <!-- steal treasure (ST) -->
              <node id="6" nodeType="AND">
                <attribute name="name">steal treasure</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">2</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- get away (GA) -->
              <node id="7" nodeType="OR">
                <attribute name="name">get away</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- thieves fleeing (TF) -->
              <node id="8" nodeType="SAND">
                <attribute name="name">thieves fleeing</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- treasure stolen (TS) -->
              <node id="9" nodeType="CAND">
                <attribute name="name">treasure stolen</attribute>
                <attribute name="role">ATTACK</attribute>
                <attribute name="attributes">
                  <attribute name="attribute">
                    <attribute name="name">cost</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                  <attribute name="attribute">
                    <attribute name="name">time</attribute>
                    <attribute name="value">0</attribute>
                  </attribute>
                </attribute>
              </node>

              <!-- ================================================ -->
              <!-- Arcs                                             -->
              <!-- ================================================ -->

              <arc id="10" arcType="arc" source="6" target="1"/> <!-- ST->b -->
              <arc id="11" arcType="arc" source="6" target="2"/> <!-- ST->f -->

              <arc id="12" arcType="arc" source="7" target="3"/> <!-- GA->h -->
              <arc id="13" arcType="arc" source="7" target="4"/> <!-- GA->e -->

              <arc id="14" arcType="arc" source="8" target="6"/> <!-- TF->ST -->
              <arc id="15" arcType="arc" source="8" target="7"/> <!-- TF->GA -->

              <arc id="16" arcType="arc" source="9" target="8"/> <!-- TS->TF -->
              <arc id="17" arcType="arc" source="9" target="5"/> <!-- TS->p -->
            </model>
          </parameters>
          <parameters xsi:type="ns2:multipleLineTextParameter" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <name>Scheduling Table</name>
            <shortname/>
            <help>String parameter help message.</help>
            <direction>OUT</direction>
            <editable>false</editable>
            <defaultValue/>
            <value/>
          </parameters>
        </parameters>
        <shortname/>
        <tool>adt2amas</tool>
        <version>0.1</version>
      </service>
    </all:asynchronous-call>
  </soapenv:Body>
</soapenv:Envelope>
```
